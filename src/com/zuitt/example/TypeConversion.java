package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args){
        System.out.println("How old are you?");
        Scanner myObj = new Scanner(System.in);

//        int age = myObj.nextInt();
//        System.out.println("Your age is: "+ age + age);

//        double age = Double.parseDouble(myObj.nextLine());
         double age = myObj.nextDouble();
        System.out.println("Your age is: "+ age);

        // Much like JS, Java also has similar mathematical operators
        System.out.println("Enter the first number: ");
        int number1 = myObj.nextInt();

        System.out.println("Enter the second number: ");
        int number2 = myObj.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of both numbers are: "+sum);

    }
}
