package com.zuitt.example;

import java.awt.*;

public class Variables {
    public static void main(String[] args){
        // variable
        int age;
        char middle_name;

        // variable initialization
        int x = 0;
        age = 18;

        /*
            int myNum = "sampleString"; -> will result to an error
            L is added at the end of the long number to be recognize as long. Otherwise, it will wrongfully recognized as int.
        */

        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // float - we have to add f at the end of a float to be recognized
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        // double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char - can only hold 1 character
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        // constants in Java - uses final keyword beside the dataType
        // final dataType VARIABLENAME
        final int PRINCIPAL= 3000;
        System.out.println(PRINCIPAL);
        // PRINCIPAL = 4000; -> error

        // String - non-primitive data type. String are actually objects that can use methods.
        String username = "theTinker23";
        System.out.println(username);
        // isEmpty is a string method which returns a boolean
        // checks the length of the string, returns if the length  is 0, returns false otherwise
        System.out.println(username.isEmpty());

    }
}
