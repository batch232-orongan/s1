package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name:");

        String firstName = myObj.nextLine();

        Scanner myObj2 = new Scanner(System.in);
        System.out.println("Last Name:");

        String lastName = myObj2.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = myObj.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = myObj.nextDouble();

        System.out.println("Good day," + firstName+ " " + lastName+ "." );

        double ave = (firstSubject + secondSubject+ thirdSubject) / 3;
        System.out.println("Your grade average is: " + ave);

    }
}
